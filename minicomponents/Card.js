import React from 'react';
import { withNavigation } from '@react-navigation/compat';
import { StyleSheet, Image, TouchableWithoutFeedback, View, Text } from 'react-native';

class Card extends React.Component {
  constructor(props){
    super(props)
  }
  render() {

    return (
      <TouchableWithoutFeedback onPress={()=>{this.props.navigation.navigate('Ressource')}}>
        <View style={styles.main_container}>
          <Image style={{width:"100%",height:"50%", resizeMode:"cover", borderTopLeftRadius:15, borderTopRightRadius:15}} source={this.props.item.image}></Image>
          <Text style={{marginTop:5, textAlign:"center", fontWeight:"bold"}}>{this.props.item.title}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  main_container:{
    elevation:10,
    borderRadius:15,
    height:150,
    width:"45%",
    backgroundColor:"white",
    margin:10
  }
});

export default withNavigation(Card);
