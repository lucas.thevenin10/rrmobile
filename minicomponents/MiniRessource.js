import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';

export default class MiniRessource extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <TouchableOpacity style={{width:"100%",alignItems:"center"}}
                onPress = {()=>{this.props.navigation.navigate("Ressource")}}>
                <View style={styles.main_container}>
                    <Image source={require("../assets/imgs/project15.jpg")}
                            style={styles.illustration}></Image>
                    <View style={{width:"60%"}}>
                        <Text style={styles.nom_ressource}>Lorem Ipsum</Text>
                        <Text style={styles.description_ressource} 
                        numberOfLines={3}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae urna in metus tincidunt semper. Pellentesque bibendum pulvinar nisi, id.
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    main_container:{
        width:"95%",
        height:150,
        elevation:10,
        borderRadius:15,
        backgroundColor:"white",
        padding:15,
        marginBottom:25,
        marginTop:25,
        flexDirection:"row",
        justifyContent:"space-between"
    },
    illustration:{
        height:"100%",
        width:"30%",
        resizeMode:"cover",
        borderRadius:15,
    },
    nom_ressource:{
        fontSize:18,
        fontWeight:"bold",
        marginBottom:10
    },
    description_ressource:{
    }
})