import {createStackNavigator} from "@react-navigation/stack";
import React from 'react';
import AccueilView from "../components/AccueilView";
const Stack = createStackNavigator(); 

class AccueilNav extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
            return(
                <Stack.Navigator
                screenOptions={{
                  }}>
                    <Stack.Screen
                        name="Accueil"
                        component={AccueilView}
                    />
                </Stack.Navigator>

            )
        
    }

}

export default AccueilNav;