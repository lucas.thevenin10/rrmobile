import {createStackNavigator} from "@react-navigation/stack";
import React from 'react';
import ChatsView from "../components/ChatsView";
import IndividualChatView from "../components/IndividualChatView";
const Stack = createStackNavigator(); 

class ChatNav extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
            return(
                <Stack.Navigator
                screenOptions={{
                  }}>
                    <Stack.Screen
                        name="Chat"
                        component={ChatsView}
                    />
                    <Stack.Screen
                        name="Message"
                        component={IndividualChatView}
                    />
                </Stack.Navigator>

            )
        
    }

}

export default ChatNav;