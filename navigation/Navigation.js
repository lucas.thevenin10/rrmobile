import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import React from 'react';
import AccueilNav from './AccueilNav';
import RechercheNav from './RechercheNav';
import FilRessourcesNav from './FilRessourcesNav';
import ChatNav from './ChatNav';
import ProfilNav from './ProfilNav';
const Tab = createBottomTabNavigator();
  
export default class MyTabs extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <Tab.Navigator
            tabBarOptions={{
                keyboardHidesTabBar:true,
            }}>
                <Tab.Screen  
                name="Accueil" 
                component={AccueilNav} 
                options={{
                    tabBarLabel: 'Accueil',
                    tabBarIcon: ({ color }) => (
                    <MaterialCommunityIcons name="home" color={color} size={26} />
                    ),
                }}
                />
                 <Tab.Screen  
                name="Recherche" 
                component={RechercheNav} 
                options={{
                    tabBarLabel: 'Recherche',
                    tabBarIcon: ({ color }) => (
                    <MaterialCommunityIcons name="magnify" color={color} size={26} />
                    ),
                }}
                />
                <Tab.Screen  
                name="Ressources" 
                component={FilRessourcesNav} 
                options={{
                    tabBarLabel: 'Ressources',
                    tabBarIcon: ({ color }) => (
                    <MaterialCommunityIcons name="book" color={color} size={26} />
                    ),
                }}
                />                
                <Tab.Screen 
                name="Chat" 
                component={ChatNav} 
                options={{
                    tabBarLabel: 'Chat',
                    tabBarIcon: ({ color }) => (
                    <MaterialCommunityIcons name="forum" color={color} size={26} />
                    ),
                }}
                />
                <Tab.Screen 
                name="Profil" 
                component={ProfilNav} 
                options={{
                    tabBarLabel: 'Profil',
                    tabBarIcon: ({ color }) => (
                    <MaterialCommunityIcons name="account" color={color} size={26} />
                    ),
                }}
                />
            </Tab.Navigator>
        );
    }
}