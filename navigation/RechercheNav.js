import {createStackNavigator} from "@react-navigation/stack";
import React from 'react';
import RechercheView from "../components/RechercheView";
import RessourcesView from "../components/RessourcesView";
import RessourceView from "../components/RessourceView";
const Stack = createStackNavigator(); 

class RechercheNav extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
            return(
                <Stack.Navigator
                screenOptions={{
                  }}>
                    <Stack.Screen
                        name="Recherche"
                        component={RechercheView}
                    />
                    <Stack.Screen
                        name="Ressources"
                        component={RessourcesView}
                    />
                    <Stack.Screen
                        name="Ressource"
                        component={RessourceView}
                    />
                </Stack.Navigator>

            )
        
    }

}

export default RechercheNav;