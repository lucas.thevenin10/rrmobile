import {createStackNavigator} from "@react-navigation/stack";
import React from 'react';
import ProfilView from "../components/ProfilView";
import RessourceView from "../components/RessourceView";
import SettingsView from "../components/SettingsView";
import TestComponent from "../components/TestComponent";
const Stack = createStackNavigator(); 

class ProfilNav extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
            return(
                <Stack.Navigator
                screenOptions={{
                  }}>
                    <Stack.Screen
                        name="Profil"
                        component={ProfilView}
                    />
                    <Stack.Screen
                        name="Paramètres"
                        component={SettingsView}
                    />
                    <Stack.Screen
                        name="Ressource"
                        component={RessourceView}
                    />
                </Stack.Navigator>

            )
        
    }

}

export default ProfilNav;