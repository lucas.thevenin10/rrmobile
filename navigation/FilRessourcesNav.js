import {createStackNavigator} from "@react-navigation/stack";
import React from 'react';
import RessourceView from "../components/RessourceView";
import RessourcesView from "../components/RessourcesView";
const Stack = createStackNavigator(); 

class FilRessourcesNav extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
            return(
                <Stack.Navigator
                screenOptions={{
                  }}>
                      <Stack.Screen
                        name="Ressources"
                        component={RessourcesView}
                    />
                    <Stack.Screen
                        name="Ressource"
                        component={RessourceView}
                    />                    
                </Stack.Navigator>

            )
        
    }

}

export default FilRessourcesNav;