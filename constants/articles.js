export default [
  {
    title: 'Lorem ipsum dolor sit amet, consectetur ',
    image: require("../assets/imgs/project13.jpg"),
    cta: 'Voir Ressource',
    horizontal: true
  },
  {
    title: 'Lorem ipsum dolor sit amet, consectetur',
    image: require("../assets/imgs/bg40.jpg"),
    cta: 'Voir Ressource'
  },
  {
    title: 'Lorem ipsum dolor sit amet, consectetur?',
    image: require("../assets/imgs/bg28.jpg"),
    cta: 'Voir Ressource'
  },
  {
    title: 'Lorem ipsum dolor sit amet, consectetur',
    image: require("../assets/imgs/project21.jpg"),
    cta: 'Voir Ressource'
  },

  {
    title: 'Lorem ipsum dolor sit amet, consectetur',
    image: require("../assets/imgs/project15.jpg"),
    cta: 'Voir Ressource',
    horizontal: true
  },
  {
    title: 'Lorem ipsum dolor sit amet, consectetur',
    image: require("../assets/imgs/saint-laurent.jpg"),
    subtitle: 'Black Jacket',
    description:
      'Lorem ipsum dolor sit amet, consectetur.',
    horizontal: true
  },
  {
    title: '$299',
    image: require("../assets/imgs/saint-laurent1.jpg"),
    subtitle: 'Saint Laurent',
    description:
      'Lorem ipsum dolor sit amet, consectetur.',
    horizontal: true
  }
];
