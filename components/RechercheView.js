import React from 'react';
import {View, Text, StyleSheet, TextInput, ScrollView, TouchableOpacity} from 'react-native';

export default class RechercheView extends React.Component{
    constructor(props){
        super(props);
        this.state={
            categories:[{
                nom:"Communication",
                actif:false,
            },
            {
                nom:"Culture",
                actif:false
            },
            {
                nom:"Développement personnel",
                actif:false
            },
            {
                nom:"Parentalité",
                actif:false
            },
            {
                nom:"Intelligence émotionnelle",
                actif:false
            },
            {
                nom:"Loisirs",
                actif:false
            }, {
                nom:"Monde Professionel",
                actif:false
            }, {
                nom:"Qualité de vie",
                actif:false
            }, {
                nom:"Recherche de sens",
                actif:false
            }, {
                nom:"Spiritualité",
                actif:false
            }, {
                nom:"Santé Physique",
                actif:false
            }, {
                nom:"Vie affective",
                actif:false
            }
        ]
            ,
            typeRelation:[{
                nom:"Soi",
                actif:false,
            },
            {
                nom:"Conjoints",
                actif:false
            },
            {
                nom:"Famille",
                actif:false
            },
            {
                nom:"Professionel",
                actif:false
            }
            ,
            {
                nom:"Inconnu",
                actif:false
            }
            ]
            ,
            typeRessource:[{
                nom:"Vidéo",
                actif:false,
            },
            {
                nom:"Activité",
                actif:false
            },
            {
                nom:"Article",
                actif:false
            },
            {
                nom:"Amis et communauté",
                actif:false
            }
            ]
            ,
            search_text:""
        }
    }
    categoriesRender(){
        let categoriesRender = [];
        for(let i=0; i<this.state.categories.length; i++){
            categoriesRender.push(
                <TouchableOpacity onPress={()=>{
                    let nextCategories = this.state.categories;
                    nextCategories[i].actif = !nextCategories[i].actif;
                    this.setState({categories : nextCategories})
                }}
                key={i}>
                    <View style={this.state.categories[i].actif?styles.filtre_actif:styles.filtre}>
                        <Text style={this.state.categories[i].actif?styles.texte_filtre_actif:styles.texte_filtre}>{this.state.categories[i].nom}</Text>
                    </View>
                </TouchableOpacity>
            )
        }
        return categoriesRender;
    }

    typeRelationRender(){
        let typeRelationRender = [];
        for(let i=0; i<this.state.typeRelation.length; i++){
            typeRelationRender.push(
                <TouchableOpacity onPress={()=>{
                    let nextTypeRelation = this.state.typeRelation;
                    nextTypeRelation[i].actif = !nextTypeRelation[i].actif;
                    this.setState({typeRelation : nextTypeRelation})
                }}
                key={i}>
                    <View style={this.state.typeRelation[i].actif?styles.filtre_actif:styles.filtre}>
                        <Text style={this.state.typeRelation[i].actif?styles.texte_filtre_actif:styles.texte_filtre}>{this.state.typeRelation[i].nom}</Text>
                    </View>
                </TouchableOpacity>
            )
        }
        return typeRelationRender;
    }

    typeRessourceRender(){
        let typeRessourceRender = [];
        for(let i=0; i<this.state.typeRessource.length; i++){
            typeRessourceRender.push(
                <TouchableOpacity onPress={()=>{
                    let nextTypeRessource = this.state.typeRessource;
                    nextTypeRessource[i].actif = !nextTypeRessource[i].actif;
                    this.setState({typeRessource : nextTypeRessource})
                }}
                key={i}>
                    <View style={this.state.typeRessource[i].actif?styles.filtre_actif:styles.filtre}>
                        <Text style={this.state.typeRessource[i].actif?styles.texte_filtre_actif:styles.texte_filtre}>{this.state.typeRessource[i].nom}</Text>
                    </View>
                </TouchableOpacity>
            )
        }
        return typeRessourceRender;
    }
    render(){
        return(
            <ScrollView style={styles.main_container}>
                <View style={styles.recherche_container}>
                    <TextInput style={styles.search_bar} placeholder="Chercher un nom de ressource"></TextInput>
                    <TouchableOpacity 
                    style={{width:"30%",alignItems:"center"}}
                    onPress={()=>{this.props.navigation.navigate("Ressources")}}>
                        <View style={styles.button_container}>
                            <Text style={styles.button_text}>Rechercher</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Text style={styles.titre_filtre}>Catégorie</Text>
                <View style={styles.filtres_container}>
                    {this.categoriesRender()}
                </View>
                <Text style={styles.titre_filtre}>Type de relation</Text>
                <View style={styles.filtres_container}>
                    {this.typeRelationRender()}
                </View>
                <Text style={styles.titre_filtre}>Type de ressource</Text>
                <View style={styles.filtres_container}>
                    {this.typeRessourceRender()}
                </View>
                
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    main_container:{
        flex:1,
        paddingBottom:30,
        paddingTop:30
    },
    recherche_container:{
        flexDirection:"row",
        alignItems:"center"
    },
    titre_filtre:{
        fontSize:20,
        marginBottom:5,
        marginTop:20,
        width:"100%",
        marginLeft:15,
    },
    filtres_container:{
        width:"100%",
        flexWrap:"wrap",
        flexDirection:"row"
    },
    filtre:{
        borderRadius:15,
        padding:5,
        height:30,
        borderColor:"#228997",
        borderWidth:1,
        margin:10
    },
    texte_filtre:{
        color:"#228997",
        fontSize:16
    },
    filtre_actif:{
        borderRadius:15,
        padding:5,
        height:30,
        backgroundColor:"#228997",
        borderWidth:1,
        borderColor:"#228997",
        margin:10
    },
    texte_filtre_actif:{
        color:"white",
        fontSize:16
    },
    search_bar:{
        width:"65%",
        marginLeft:15,
        borderColor:"#228997",
        borderWidth:1,
        height:40,
        borderRadius:10,
        padding:10,
    },
    button_container:{
        backgroundColor:"#228997",
        padding:10,
        flex:1,
        alignItems:"center",
        borderRadius:15      
    },
    button_text:{
        color:"white",
        fontSize:18
    }
})