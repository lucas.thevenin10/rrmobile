import React from 'react';
import {View, Text, StyleSheet, TextInput, KeyboardAvoidingView,Platform, TouchableWithoutFeedback, Keyboard, ScrollView} from 'react-native';


const Message = (props) => {
    return (
        <View style={styles.message_envoye_container} >
            <Text style={styles.message_envoye_text}>{props.content}</Text>
        </View>
    );
  }
  


export default class ChatsView extends React.Component{
    constructor(props){
        super(props);
        this.state={
            rows:["Salut", "Réponds moi wsh"]
        }
    }
     

        render(){
            
            let msgs_array=[];
            this.state.rows.forEach(function(e,i){
                msgs_array.push(<Message key={i} content={e}></Message>);
            }.bind(this)); 
            return(
            
            <KeyboardAvoidingView keyboardVerticalOffset = {110}
            style={styles.main_container} behavior={Platform.OS === "ios" ? "padding" : null}
            style={{ flex: 1 }}>
                <ScrollView  
                            ref={ref => {this.scrollView = ref}}
                            onContentSizeChange={() => this.scrollView.scrollToEnd({animated: true})}>

                    { msgs_array }
                    </ScrollView>
                <View style={styles.text_input_class} >
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <TextInput  ref={input => { this.textInput = input }}
                                    clearButtonMode="always"
                                    placeholder="Tapez votre message" 
                                    style={{ height: 70, borderColor: 'gray', backgroundColor:"white", borderWidth: 1}} 
                                    onSubmitEditing={(event) => this.sendMessage(event.nativeEvent.text)}/>
                    </TouchableWithoutFeedback>
                </View>
            </KeyboardAvoidingView>
        )
    }

    sendMessage(t){
        let newRows = this.state.rows;
        newRows.push(t);
        this.setState({rows:newRows})
        this.textInput.clear()
    }
}


const styles = StyleSheet.create({
    main_container:{
        flex:1,
        height:'100%',
        marginTop:30
    },
    message_envoye_container:{
        height:60,
        backgroundColor: '#1084ff',
        justifyContent :'flex-end',
        margin:10,
        borderRadius: 15,
        borderBottomRightRadius:0
    },
    message_envoye_text:{
        paddingTop: 7, 
        paddingLeft:10,
        color: 'white',
        flex:1
    },
    text_input_class:{
        flex: 1,
        justifyContent:'flex-end',
        zIndex:3
        
    }
})
