import React from 'react';
import {View, Text, StyleSheet, Switch} from 'react-native';

export default class SettingsView extends React.Component{
    constructor(props){
        super(props);
    }
   

    render(){
        return(
            <View style={styles.main_container}>
                <View style={styles.switch_flex}>
                    <Switch style={styles.switch}>

                    </Switch>
                    <Text>Mode Sombre</Text>
                </View>
                <View style={styles.switch_flex}>
                    <Switch style={styles.switch}>

                    </Switch>
                    <Text>Notifications</Text>
                </View>
                <View style={styles.switch_flex}>
                    <Switch style={styles.switch}>

                    </Switch>
                    <Text>Messages d'inconnus</Text>
                </View>
                <View style={styles.switch_flex}>
                    <Switch style={styles.switch}>
                    </Switch>
                    <Text>Assistant vocal</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container:{
        flex:1,
        marginTop:30,
    },
    switch_flex: {
        width: "50%",
        flexDirection: "row",
        alignItems: 'center',
        marginTop: 25
    },
    switch: {
        marginLeft: 30,
        marginRight: 15
    }
})