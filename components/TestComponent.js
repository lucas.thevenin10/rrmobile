import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class TestComponent extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <View style={styles.main_container}>
                <Text>Ceci est la test view</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container:{
        flex:1,
        marginTop:50
    }
})