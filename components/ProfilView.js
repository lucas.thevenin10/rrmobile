import React from 'react';
import { View, StyleSheet, Dimensions, ScrollView, Image, ImageBackground, Text, Button } from 'react-native';

import  Card from '../minicomponents/Card';
import { Images, nowTheme } from '../constants';
import articles from "../constants/articles";

const { width, height } = Dimensions.get('screen');

const thumbMeasure = (width - 48 - 32) / 3;

export default class ProfilView extends React.Component {
  constructor(props) {
    super(props);
  }


  renderArticles = () => {
    return (
      <View
        style={{width:"100%", flexDirection:"row", flexWrap:"wrap"}}
      >
        <Card item={articles[0]} horizontal />
        <Card item={articles[1]}/>
        <Card item={articles[2]} />
        <Card item={articles[3]} horizontal />
        <Card item={articles[4]} full />
      </View>
    );
  };

  
  render() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
      }} >
        <ScrollView showsVerticalScrollIndicator={false}>
        <View flex={0.6} >
          <ImageBackground
            source={Images.ProfileBackground}
            style={styles.profileContainer}
            imageStyle={styles.profileBackground}
          >
            <View flex style={styles.profileCard}>
              <View style={{  width: "100%", zIndex: 5, paddingHorizontal: 20 }}>
                <View  style={{ top: height * 0.15, width:"100%", alignItems:"center" }}>
                  <Image source={Images.ProfilePicture} style={styles.avatar} />
                </View>
                <View style={{ top: height * 0.2 }}>
                  <View style={{width:"100%", alignItems:"center"}} >
                    <Text
                      style={{
                        marginBottom: 10,
                        fontWeight: '900',
                        fontSize: 26,
                        color:'#ffffff'
                      }}
                      
                    >
                      John Doe
                        </Text>

                    <Text
                      size={16}
                      style={{
                        marginTop: 5,
                        lineHeight: 20,
                        fontWeight: 'bold',
                        fontSize: 18,
                        opacity: .8,
                        color:"white"
                      }}
                    >
                      Test
                        </Text>
                  </View>
                  <View style={styles.info}>
                    <View style={{ flexDirection:"row", justifyContent:"space-around"}}>

                      <View >
                        <Text
                          size={18}
                          style={{ marginBottom: 4, color:"white" }}
                        >
                          2K
                            </Text>
                        <Text style={{color:"white" }}>
                          Toto
                            </Text>
                      </View>

                      <View >
                        <Text
                          color="white"
                          size={18}
                          style={{ marginBottom: 4, color:"white" }}
                        >
                          26
                            </Text>
                        <Text size={14} 
                        style={{color:"white" }}>
                          Tata
                              </Text>
                      </View>

                      <View >
                        <Text
                          color="white"
                          size={18}
                          style={{ marginBottom: 4, color:"white" }}
                        >
                          48
                            </Text>
                        <Text size={14} style={{color:"white" }}>
                          Bobo
                            </Text>
                      </View>

                    </View>
                  </View>
                </View>

              </View>


              <View
                style={{ width: width }}
              >
              </View>
            </View>
          </ImageBackground>


        </View>
        <View />
        <View flex={0.4} style={{ padding:15, marginTop: 90 }}>
            <View flex style={{ marginTop: 30 }}>
              <View style={{width:"100%", justifyContent:"center", alignItems:"center"}}>
                <Text
                  style={{
                    color: '#2c2c2c',
                    fontWeight: 'bold',
                    fontSize: 19,
                    marginTop: 15,
                    marginBottom: 30,
                    zIndex: 2
                  }}
                >
                  About me
                        </Text>
                <Text
                  size={16}
                  muted
                  style={{
                    textAlign: 'center',
                    zIndex: 2,
                    lineHeight: 25,
                    color: '#9A9A9A',
                    paddingHorizontal: 15
                  }}
                >
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                  Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor.
                   Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi.
                    Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat.
                        </Text>
              </View>
              <View style={{ paddingVertical: 14, paddingHorizontal: 15 }} space="between">
                <Text bold size={16} color="#2c2c2c" style={{ marginTop: 3 }}>
                  Resources
                        </Text>
               
              </View>

              <View style={styles.home}>
                {this.renderArticles()}
              </View>

            </View>
        </View>
        </ScrollView>
      </View>

    )
  }
}

const styles = StyleSheet.create({
  profileContainer: {
    height: height * 0.4,
    padding: 0,
    zIndex: 1
  },
  profileBackground: {
    height: height * 0.55
  },

  info: {
    marginTop: 30,
    paddingHorizontal: 10,
    height: height * 0.8
  },

  profileCard: {
    height: height*0.3
  },
  avatarContainer: {
    position: 'relative',
    marginTop: -80
  },
  avatar: {
    width: thumbMeasure,
    height: thumbMeasure,
    borderRadius: 50,
    borderWidth: 0
  },
  nameInfo: {
    marginTop: 35
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure
  },
  social: {
    width: nowTheme.SIZES.BASE * 3,
    height: nowTheme.SIZES.BASE * 3,
    borderRadius: nowTheme.SIZES.BASE * 1.5,
    justifyContent: 'center',
    zIndex: 99,
    marginHorizontal: 5
  },
  home: {
    width: width
  }
})