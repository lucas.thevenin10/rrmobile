import React from 'react';
import {ScrollView, Text, StyleSheet, View} from 'react-native';
import MiniRessource from '../minicomponents/MiniRessource';

export default class RessourcesView extends React.Component{
    constructor(props){
        super(props);
    }
    listeRessourceRender(){
        let listRessources = [];
        for(let i = 0; i<15; i++){
            listRessources.push(<MiniRessource key={i} navigation={this.props.navigation}></MiniRessource>)
        }
        return listRessources;
    }
    render(){
        return(
            <ScrollView style={styles.main_container}>
                <View style={styles.container}>
                    {this.listeRessourceRender()}
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    main_container:{
        width:"100%",
        height:"100%",
        paddingTop:30,
    },
    container:{
        width:"100%",
        height:"100%",
        alignItems:"center",
    }
})