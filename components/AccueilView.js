import React from 'react';
import {View, Text, StyleSheet, Button, IconButton, TouchableOpacity} from 'react-native';
import { BorderlessButton } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class AccueilView extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <View style={styles.main_container}>
                <View style={styles.header}>
                    <TouchableOpacity>
                        <Icon style={styles.icon_} onPress={() => (this.props.navigation.navigate('Accueil'))} name="home" color="#228997" backgroundColor="none" size={30}>
                        </Icon>
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <Icon style={styles.icon_} onPress={() => (this.props.navigation.navigate('Profil'))}  name="user" color="#228997" backgroundColor="none" size={30}>
                        </Icon>
                    </TouchableOpacity>
                </View>

                <TouchableOpacity onPress={() => (this.props.navigation.navigate('Ressources'))}>
                    <View style={styles.btn} >
                        <Text style={styles.text_style} >Ressources</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => (this.props.navigation.navigate('Recherche'))}>
                    <View style={styles.btn} >
                        <Text style={styles.text_style} >Recherche</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => (this.props.navigation.navigate('Chat'))}>
                    <View style={styles.btn} >
                        <Text style={styles.text_style} >Messagerie</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => (this.props.navigation.navigate('Profil'))}>
                    <View style={styles.btn} >
                        <Text style={styles.text_style} >Profil</Text>
                    </View>
                </TouchableOpacity>

               <TouchableOpacity onPress={() => (this.props.navigation.navigate('Ressources'))}>
                    <View style={styles.btn} >
                        <Text style={styles.text_style} >Ajouter une Ressources</Text>
                    </View>
                </TouchableOpacity>
                
                <TouchableOpacity onPress={() => (this.props.navigation.navigate('Paramètres'))}>
                    <View style={styles.btn} >
                        <Text style={styles.text_style}>Paramètre</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container:{
        flex:1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    btn: {
        width: 319,
        height: 54,
        backgroundColor: '#228997',
        alignItems: 'center',
        borderRadius: 5,
        justifyContent : 'center',
        alignItems: 'center'
    },
    text_style: {
        color: 'white'
    },
    header: {
        flexDirection: 'row',
        width: "100%",
        justifyContent: 'space-between'
    },
    icon_: {
        marginEnd: 20,
        marginStart: 20
    }
})