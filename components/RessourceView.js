import React from 'react';
import {View, ScrollView, Text, StyleSheet, Image} from 'react-native';

export default class RessourceView extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <ScrollView style={styles.main_container}>
                 <Image source={require("../assets/imgs/project15.jpg")}
                            style={styles.illustration}></Image>
                    <Text style={styles.title}>
                        Lorem Ipsum Dolor
                    </Text>
                    <Text style={styles.text}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam facilisis metus neque, sed feugiat sapien rhoncus vel. Donec viverra mauris at ligula bibendum dapibus. Sed et mollis velit. Nunc a nulla id mauris dapibus finibus. Duis eget erat ut lectus bibendum pretium vitae ac velit. Vivamus vestibulum sem enim, vitae lacinia turpis fermentum sed. Nam dapibus neque vel gravida consectetur. Nulla pharetra odio in finibus accumsan.

Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis consectetur justo et leo faucibus, placerat lobortis lacus iaculis. Praesent ut auctor lectus, sit amet bibendum metus. Nullam vitae ex tortor. In risus felis, ultricies id urna in, tempor pellentesque sapien. Quisque lacinia, risus sed scelerisque porta, sapien purus egestas massa, eget viverra augue erat placerat dui. Quisque ac tincidunt lacus, non aliquam elit. Praesent dapibus, arcu efficitur convallis congue, enim nulla aliquet augue, in vestibulum ipsum risus sit amet dui.

Mauris iaculis tortor neque, rutrum fringilla nisi vulputate nec. Donec maximus dolor in justo mattis facilisis. Sed in diam id ipsum cursus ullamcorper. Praesent ac urna ac lacus lacinia finibus ut a odio. Nunc odio nulla, facilisis in mattis at, suscipit sit amet mauris. Suspendisse efficitur ex tellus, eget posuere enim aliquam id. Donec sed dignissim purus. Nam sit amet ante at odio sodales ultricies id ac tellus. Nunc ut eleifend enim. Cras magna orci, dapibus ut ligula vitae, vulputate faucibus ipsum. Nullam congue augue at ligula placerat aliquet. Mauris pellentesque tellus at velit rutrum, facilisis faucibus ipsum hendrerit. Sed at blandit turpis. Aliquam luctus est sed mollis scelerisque.
                    </Text>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    main_container:{
        flex:1,
    },
    illustration:{
        width:"100%",
    },
    title:{
        fontSize:22,
        fontWeight:"bold",
        marginTop:15,
        textAlign:"center"
    },
    text:{
        padding:25,
        textAlign:"justify"
    }
})