import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class IndividualChatView extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <View style={styles.main_container}></View>
        )
    }
}

const styles = StyleSheet.create({
    main_container:{
        flex:1,
        marginTop:30
    }
})